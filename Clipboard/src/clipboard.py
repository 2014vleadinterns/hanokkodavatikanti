import sys
import tempfile

__all__ = ["copytext", "copyblob", "gettext", "getblob", "reset"]

__text__ = None
__blob__ = None
__sizet__ = 20
__sizeb__ = 100

def copytext(text):
    global __text__
    global __sizet__
    if len(text) < __sizet__:
	__text__ = text
    else:
	__text__ = tempfile.TemporaryFile()
	__text__.write(text)
	f.close()
	
def copyblob(blob):
    global __blob__
    global __sizeb__
    if len(blob) < __sizeb__:
	__blob__ = blob
    else:
	__blob__ = tempfile.TemporaryFile()
	__blob__.write(blob)
	f.close()

def gettext():
    global __text__
    if isinstance(__text__,file):
	__text__.seek(0)
	__text__.read()
    else:
	return __text__
    
def getblob():
    global __blob__
    if isinstance(__blob__,file):
	__text__.seek(0)
	__text__.read()
    else:
	return __blob__

def reset():
    global __text__, __blob__
    __text__ = None
    __blob__ = None

##
## -------------------------------------------------------------
##
__observers__ = []
def addobserver(observer):
    __observers__.append(observer)

def removeobserver(observer):
    try:
        __observers__.remove(observer)
    except ValueError, TypeError:
        pass

def notify(reason):
    for observer in __observers__:
        if observer is not None:
            try:
                observer(reason)
            except TypeError:
                pass
