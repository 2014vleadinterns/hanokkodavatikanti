# -*- coding: UTF-8 -*-
import clipboard

def run_clipboard_tests():
    def test_empty_clipboard():
        assert(clipboard.gettext() == None)
        assert(clipboard.getblob() == None)
    
    def test_reset_clipboard():
        clipboard.reset()
        assert(clipboard.gettext() == None)
        assert(clipboard.getblob() == None)

    def test_copy_hindi_text():
        clipboard.reset()
        msg = u'विकिपीडिया:इण्टरनेट पर हिन्दी के साधन'
        clipboard.copytext(msg)
        text = clipboard.gettext()
        assert(msg == text)

    def test_copy_english_text():
        clipboard.reset()
        clipboard.copytext("hello, world!")
        assert(clipboard.getblob() == None)

    def test_reset_only_text():
	clipboard.reset()
	clipboard.copytext("Hello World!")
	clipboard.reset()
	assert(clipboard.gettext() == None)
	assert(clipboard.getblob() == None)

    def test_text_after_only_text():
	clipboard.reset()
	clipboard.copytext("Hello World!")
	clipboard.copytext("Hello World!")
	#assert(clipboard.gettext() == None)
	assert(clipboard.getblob() == None)

    def test_blob_after_only_text():
	clipboard.reset()
	clipboard.copytext("hey")
	f = open("a.jpg","rb")
	binary = f.read()
	f.close()	
	clipboard.copyblob(binary)
	#assert(clipboard.gettext() == None)
	#assert(clipboard.getblob() == None)

    def test_only_blob():
	clipboard.reset()
	f = open("a.jpg","rb")
	binary = f.read()
	f.close()
	clipboard.copyblob(binary)
	assert(clipboard.gettext() == None)
	#assert(clipboard.getblob() == None)

    def test_reset_only_blob():
	clipboard.reset()
	f = open("a.jpg","rb")
	binary = f.read()
	f.close()
	clipboard.copyblob(binary)
	clipboard.reset()
	assert(clipboard.gettext() == None)
	assert(clipboard.getblob() == None)

	

    def test_text_after_only_blob():
	clipboard.reset()
	f=open("a.jpg","rb")
	binary=f.read()
	f.close()
	clipboard.copyblob(binary)
	clipboard.copytext("hello")
	#assert(clipboard.gettext() == None)
	#assert(clipboard.getblob() == None)

    def test_reset_text_after_only_blob():
	clipboard.reset()
	f=open("a.jpg","rb")
	binary=f.read()
	f.close()
	clipboard.copyblob(binary)
	clipboard.copytext("hello")
	clipboard.reset()
	assert(clipboard.gettext() == None)
	assert(clipboard.getblob() == None)

    def test_all():
	clipboard.reset()
	

	

    test_empty_clipboard()
    test_reset_clipboard()
    test_copy_english_text()
    #test_copy_hindi_text()
    test_reset_only_text()
    test_text_after_only_text()
    test_blob_after_only_text()
    test_only_blob()
    test_reset_only_blob()
    test_text_after_only_blob()
    test_reset_text_after_only_blob()
    

run_clipboard_tests()


def run_clipboard_observer_tests():
    def test_one_observer():
        def anobserver(reason):
            print "observer notified. reason: ", reason

        clipboard.reset()
        clipboard.addobserver(anobserver)
        clipboard.copytext("hello, world!")
    
    test_one_observer()

run_clipboard_observer_tests()
