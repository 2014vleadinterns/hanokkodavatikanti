import note

def test_empty_note():
    assert(note.get_notes() == [])

def test_insert_into_empty_note():
    t=note.get_notes()
    c=t[:]
    b=note.new_note( 'x','y')
    assert(c!= b)

def test_insert_empty_note():
    t=note.get_notes()
    c=t[:]
    b=note.new_note( None , None)
    assert(c!=b)

def test_insert_non_empty_note():
    note.new_note('x','y')
    b=note.new_note('y','z')
    assert((len(b)-1)!=0)

def test_delete_from_non_empty_note():
    note.new_note('f','y')
    b=note.new_note('y','z')
    k=b[:]
    c=note.del_note('y')
    assert(k != c)

def test_search():
    note.new_note('xx','yy')
    note.find_notes('x')

test_empty_note()
test_insert_into_empty_note()
test_insert_empty_note()
test_insert_non_empty_note()
test_delete_from_non_empty_note()
test_search()
