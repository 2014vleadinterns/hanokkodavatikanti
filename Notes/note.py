import re
from string import Template
import json

__all__=["new_note","del_note","find_notes","get_notes","save_notes","load_notes"]

__notes__=[]
__i__ = 0

def new_note(title,body):
   global __notes__,__i__
   if title == None:
	title = "untitle"+`__i__`
   tuple1 = (title,body)
   __notes__.append(tuple1)
   return __notes__

def del_note(title):
   global __notes__
   for v in xrange((len(__notes__)-1),-1,-1):
      p=re.match(title,__notes__[v-1][0])
      if p!= None:
	del __notes__[v-1]
   return __notes__

def find_notes(key):
   global __notes__
   r=[]
   for v in range(len(__notes__)):
	p=re.search(key,__notes__[v-1][0])
        if __notes__[v-1][1]!=None:
	    q=re.search(key,__notes__[v-1][1])
	if p!=None or q!=None:
	    r.append(__notes__[v-1])
   return r

def get_notes():
   global __notes__
   return __notes__

def save_notes(name):
   global __notes__
   ############html format#########
   f=open(name+'.html','w+r')
   f.write('<html><head>Notes</head><body>')
   template = Template("<h1>${name}</h1>")
   template1 = Template("<p>${body}</p>")
   for tup in __notes__:
        f.write(template.substitute(dict(name=tup[0])))
        f.write(template1.substitute(dict(body=tup[1]))) 
        f.write('</body></html>')
   f.close()
   ###########JSON format#########
   json.dump(q,open(name+'.json','wb'))

def load_notes(name):
   global __notes__
   print json.load(open(js2.json))

