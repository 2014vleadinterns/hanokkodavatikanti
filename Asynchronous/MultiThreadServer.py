import time
import threading
import logging
import urlparse

import tornado.httpserver
import tornado.web
import tornado.websocket
import tornado.locale
import tornado.ioloop
from tornado import gen
from random import randint

__i__=0
__array__=[]

def external_call(handler, write, finish):
	print "\nTHREAD: ,sleeping %s" %time.time()	
	time.sleep(30)
	print "\nTHREAD: done"
	handler.write("done")
	handler.finish()	

class Handler(tornado.web.RequestHandler):
	def perform(self,a,j):
		r=randint(1,10)
		#print "process %d with time %d at %s" % (j,r,time.time())
		time.sleep(r)
		#print "%d after sleep  %s" %(j,time.time())
		__array__.append(j)
		print __array__
		#print a
		self.finish()

	@tornado.web.asynchronous
	#def get(self):
	#	print "\nMAIN:  %s" %time.time()
		# response = yield gen.Task(external_call)
		# self.write(response)
		# self.finish()	
	#	p = threading.Thread(target=external_call,args=(self, Handler.write, Handler.finish))
		#t = threading.Thread(target=Handler.perform,args=(self,))
	#	p.start()
	#	print "\nMAIN:  %s" %time.time()

	def post(self):
		global __i__
		global __array__
		post_data = dict(urlparse.parse_qsl(self.request.body))
		print post_data
		__i__=__i__+1
		p = threading.Thread(target=self.perform,args=(__i__,__i__,))
		p.start()

class IndexHandler(tornado.web.RequestHandler): 
	def get(self):
		self.write("It is not blocked!")
		self.finish()

application = tornado.web.Application([
	(r"/", Handler),
	(r"/in",IndexHandler),

])

if __name__ == "__main__":

	http_server = tornado.httpserver.HTTPServer(application)
	http_server.listen(8888)
	http_server.start(1)
	tornado.ioloop.IOLoop.instance().start()
