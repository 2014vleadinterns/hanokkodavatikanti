import re

__p__=[]

def get_inst(file1):
   global __p__
   f=open(file1,'r')
   n=0
   for line in f:
	l=str.split(line)
	__p__.append(l)
	n=n+1
   f.close()

def operator():
   global __p__
   n=1
   for line in __p__:
	if re.match('^[/*+-]$',line[0]) is None:
	    print 'error in line',n
	    exit()
	n=n+1

def operands():
   global __p__
   n=1
   for line in __p__:
	if re.match('[0-9]+$|^-[0-9]+$',line[1]) is None:
	    if re.match('[a-zA-Z][\w]*',line[1]) is None:
		print 'error in line'+str(n)+'operand 1'
		exit()
	    else:
		rand1=0
	else:
	    rand1=2

	if re.match('[0-9]+$|^-[0-9]+$',line[2]) is None:
	    if re.match('[a-zA-Z][\w]*',line[2]) is None:
		print 'error in line'+str(n)+'operand 2'
		exit()
	    else:
		rand2=0
	else:
	    rand2=2

	if re.match('[0-9]+$|^-[0-9]+$',line[3]) is None:
	    if re.match('[a-zA-Z][\w]*',line[3]) is None:
		print 'error in line'+str(n)+'result'
		exit()
	    else:
		if rand1 != 0 and rand2 != 0:
		    print 'error in line'+str(n)+'result'
	else:
	    if rand1!=2 or rand2!=2:
		    print 'error in line'+str(n)+'result'
	n=n+1

get_inst('f.txt')
operator()
operands()
